{**
 * 2007-2017 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2017 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="quickview-modal-{$product.id}-{$product.id_product_attribute}" class="modal fade quickview" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog" role="document">
   <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div class="modal-body">
      <div class="row">
        <div class="col-md-6">
			{block name='product_cover_thumbnails'}
			<div class="images-container">
			  {block name='product_cover'}
				<div class="product-cover">
				  <img class="js-qv-product-cover" src="{$product.cover.bySize.large_default.url}" alt="{$product.cover.legend}" title="{$product.cover.legend}" style="width:100%;" itemprop="image">
				  <div class="layer hidden-sm-down" data-toggle="modal" data-target="#product-modal">
					<i class="material-icons zoom-in">&#xE8FF;</i>
				  </div>
				</div>
			  {/block}

			  {block name='product_images'}
				<div class="js-qv-mask mask pos_content">
				  <ul class="product-images js-qv-product-images owl-carousel">
				  {foreach from=$product.images item=image name=myLoop}
					{if $smarty.foreach.myLoop.index % 4 == 0 || $smarty.foreach.myLoop.first }
					  <div class="thumb-container">
					  {/if}
						<img
						  class="thumb js-thumb {if $image.id_image == $product.cover.id_image} selected {/if}"
						  data-image-medium-src="{$image.bySize.medium_default.url}"
						  data-image-large-src="{$image.bySize.large_default.url}"
						  src="{$image.bySize.cart_default.url}"
						  alt="{$image.legend}"
						  title="{$image.legend}"
						  width="100"
						  itemprop="image"
						>
					{if $smarty.foreach.myLoop.iteration % 4 == 0 || $smarty.foreach.myLoop.last  }
					</div>
					{/if}
					{/foreach}
				  </ul>
				</div>
			  {/block}
			</div>
			{hook h='displayAfterProductThumbs'}
			{/block}
        </div>
        <div class="col-md-6">
          <h1 class="h1 namne_details">{$product.name}</h1>
		    <p class="reference">{l s='Reference' d='Shop.Theme.Catalog'}: {$product.reference}</p>
			{hook h='DisplayReviewsProduct'}
          {block name='product_prices'}
            {include file='catalog/_partials/product-prices.tpl'}
          {/block}
		  <div class="product-information">
          {block name='product_description_short'}
            <div id="product-description-short" class="product-desc" itemprop="description">{$product.description_short|truncate:400:' ...'|escape:'html':'UTF-8' nofilter}</div>
          {/block}
          {block name='product_buy'}
            <div class="product-actions">
              <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                <input type="hidden" name="token" value="{$static_token}">
                <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">
                {block name='product_variants'}
                  {include file='catalog/_partials/product-variants.tpl'}
                {/block}

                {block name='product_add_to_cart'}
                  {include file='catalog/_partials/product-add-to-cart.tpl'}
                {/block}

                {block name='product_refresh'}
                  <input class="product-refresh" data-url-update="false" name="refresh" type="submit" value="{l s='Refresh' d='Shop.Theme.Actions'}" hidden>
                {/block}
            </form>
          </div>
        {/block}
		</div>
		<div class="modal-footer">
		   {hook h='displayProductAdditionalInfo' product=$product}
		</div>
        </div>
      </div>
     </div>
  
   </div>
 </div>
</div>
<script type="text/javascript"> 
		$(document).ready(function() {
			var owl = $(".quickview .images-container .product-images");
			owl.owlCarousel({
				autoPlay : false ,
				smartSpeed: 1000,
				autoplayHoverPause: true,
				nav: true,
				dots : false,	
				responsive:{
					0:{
						items:1,
					},
					480:{
						items:1,
					},
					768:{
						items:1,
						nav:false,
					},
					992:{
						items:1,
					},
					1200:{
						items:1,
					}
				}
			}); 
		});
		
</script>