{if $testimonials}
<div class="testimonials_container">

	<div class="pos_title">
		<h2>{l s='Client Testimonials' mod='postestimonials'}</h2>  
	</div>
	<div class="row pos_content">
		<div class="testimonialsSlide owl-carousel">
		  {foreach from=$testimonials name=myLoop item=testimonial}
			{if $testimonial.active == 1}
				{if $smarty.foreach.myLoop.index % 1 == 0 || $smarty.foreach.myLoop.first }
				<div class="item-testimonials">
				{/if}	
					<div class="item">	
						<div class="content_author">	
							<img src="{$mediaUrl}{$testimonial.media}" alt="Image Testimonial">																			
						</div>	
						<div class="content_des">
							<div class="des_testimonial">{$testimonial.content|truncate:180:' ...'|escape:'html':'UTF-8'}</div>
							<div class="des_namepost">{$testimonial.name_post}</div>
							<div class="des_email">{$testimonial.email}</div>
						</div>							
										
					</div>
				{if $smarty.foreach.myLoop.iteration % 1 == 0 || $smarty.foreach.myLoop.last  }
				</div>
				{/if}
			{/if}
		  {/foreach}
		</div>
	</div>

</div>
<script type="text/javascript">
$(document).ready(function() {
	var testi = $(".testimonialsSlide");
		testi.owlCarousel({
		autoplay :false,
		autoplayHoverPause: true,
		smartSpeed : 1000,
		nav :false,
		dots : true,
		responsiveClass:true,
		responsive : {
		  0 : {
              items: 1
          },
          576 : {
              items: 2
          },
          768 : {
              items: 2
          },
          992 : {
              items: 1
          },
		  1200 : {
              items: 1
          }
		}
	});
});
</script>

 {/if}