$(document).ready(function(){

     $('#calculate').hide()

     var alertMsg = function($title, $msg, $type, $showButtons=true){
          Swal.fire({
               title: $title,
               text: $msg,
               icon: $type,
               showCancelButton: false,
               showConfirmButton: $showButtons
          })
     }

     var mts2, totalpriceMts2, varAncho, varAlto
     var productPrice = $("span[itemprop='price']").attr('content')

     var ancho = $('.AnchoCalc')
     var alto = $('.AltoCalc')

     var fixmeTop = $('.product-prices').offset().top
     
     /** 
     * Funcion para la validacion de los input solo numeros y 2 decimales
     */
     $.fn.inputFilter = function(inputFilter) {
          return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
          if (inputFilter(this.value)) {
               this.oldValue = this.value;
               this.oldSelectionStart = this.selectionStart;
               this.oldSelectionEnd = this.selectionEnd;
          } else if (this.hasOwnProperty("oldValue")) {
               this.value = this.oldValue;
               this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
          } else {
               this.value = "";
          }
          });
     };
     
     $(".calcM2").inputFilter(function(value) {
          return /^-?\d*[.]?\d{0,2}$/.test(value);
     });

     /**
     * Funcion para calcular la cantidad centimetros cuadrados y convertirlos a metros cuadrados
     */
     function calculateM(varAncho, varAlto) {
          mts2 = ((varAncho * varAlto)/10000)
          totalpriceMts2 = Math.round(((mts2 * productPrice) + Number.EPSILON) * 100 ) / 100
          $("span.productTotal").html('$ '+(totalpriceMts2.toFixed(2)))
          $("input.productTotal").val((totalpriceMts2.toFixed(2)))
          $("input.mts2").val(mts2)
          ancho.val(varAncho)
          alto.val(varAlto)
     }

     /**
      * Funcion para agregar las comas y speradores de unidades
     */
     function addCommas(nStr) {
          nStr += '';
          var x = nStr.split('.');
          var x1 = x[0];
          var x2 = x.length > 1 ? ',' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
               x1 = x1.replace(rgx, '$1' + '.' + '$2');
          }
          return x1 + x2;
     }

     /**
      * Funcion para detectar cambios en las variantes
      */
     $('.variants').change(function(){
          $('#calculate').show('250')
          $('#addToCart').hide('250')
     })

     ancho.on('keyup', function(){
          varAncho = parseFloat($(this).val())
          if(!varAncho)
               console.log('No se llamo la funcion')
          else if(varAncho !== '' && !varAlto)
               calculateM(varAncho, 0)
          else if(varAncho !== '' && varAlto > 0)
               calculateM(varAncho, varAlto)
     })

     alto.on('keyup', function(){
          varAlto = parseFloat($(this).val())
          if(varAncho !== ''){
               if(!varAlto)
                    console.log('No se llamo la funcion')
               else if(varAlto !== '')
                    calculateM(varAncho,varAlto)
          }
     })

     /**
      * Funcion para dejar fijo el precio al top de la pagina al hacer scroll
      */
     $(window).scroll(function() {
          var currentScroll = $(window).scrollTop();
          if (currentScroll >= fixmeTop) {
               $('.product-prices').addClass('fixedPrice');
          } else {
               $('.product-prices').removeClass('fixedPrice');
          }
     });

     /** 
      * Funciones personalizadas para el envio de datos
      */

     $('#calculate').click(function(e){
          e.preventDefault();
          let form = $('#add-to-cart-or-refresh')[0]
          let data = new FormData(form)
          $.ajax({
               url: '../../../../_serverData/_querys/_getData.php',
               data: data,
               cache: false,
               dataType: 'json',
               type: 'post',
               processData: false,  // tell $ not to process the data
               contentType: false,   // tell $ not to set contentType
               beforeSend: function(){
                    alertMsg('Calculando', 'Calculando presupuesto', 'info', false)
               },
               success: function(response){
                    $(".productTotal").html('$ '+ response)
                    alertMsg('Exito', 'Presupuesto calculado', 'success', true)
                    $('#calculate').hide('250')
                    $('#addToCart').show('250')
               }
          })
          return false;
     })
})