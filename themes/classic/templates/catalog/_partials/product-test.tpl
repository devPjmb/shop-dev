<section class="product-customization" style="margin:0!important;">
     <form method="post" action="#" enctype="multipart/form-data" name="newForm" id="newForm">
          <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
          <div
               class="product-price h1 {if $product.has_discount}has-discount{/if}"
               itemprop="offers"
               itemscope
               itemtype="https://schema.org/Offer"
          >
               <link itemprop="availability" href="{$product.seo_availability}"/>
               <meta itemprop="priceCurrency" content="{$currency.iso_code}">
               <div class="current-price">
                    <span itemprop="price" content="{$product.rounded_display_price}" hidden>{$product.price}</span>
                    <span class="productTotal">$ 0,00</span>
                    <input class="productTotal" hidden value="" name="totalPrice">
                    <input class="mts2" value="" name="mts2" hidden>
               </div>
          </div>
          <ul class="clearfix row">
               {foreach from=$customizations.fields item="field"}
               <li class="product-customization-item {if $field.type == 'text'}col-lg-6 {else} col-lg-12{/if}">
                    <label> {$field.label}</label>
                    {if $field.type == 'text'}
                    <input  
                         placeholder="{l s="98.50" d='Shop.Forms.Help'}" 
                         class="product-message calcM2 {$field.label}Calc" 
                         maxlength="250"
                         {if $field.required} required {/if} 
                         name="medidas[{$field.label}]"
                    />
                    {elseif $field.type == 'image'}
                    {if $field.is_customized}
                    <br>
                    <img src="{$field.image.small.url}">
                    <a class="remove-image" href="{$field.remove_image_url}" rel="nofollow">{l s='Remove Image' d='Shop.Theme.Actions'}</a>
                    {/if}
                    <span class="custom-file">
                    <span class="js-file-name">{l s='No selected file' d='Shop.Forms.Help'}</span>
                    <input class="file-input js-file-input" {if $field.required} required {/if} type="file" name="archivo">
                    <button class="btn btn-primary">{l s='Choose file' d='Shop.Theme.Actions'}</button>
                    </span>
                    <small class="float-xs-right">{l s='.png .jpg .pdf' d='Shop.Forms.Help'}</small>
                    {/if}
               </li>
               {/foreach}
          </ul>
          <div class="product-variants row">
               {foreach from=$groups key=id_attribute_group item=group}
                    {if !empty($group.attributes)}
                         <div class="clearfix product-variants-item col-lg-6">
                              <span class="control-label">{$group.name}</span>
                              {if $group.group_type == 'select'}
                                   <select
                                   class="form-control form-control-select variants"
                                   id="group_{$id_attribute_group}"
                                   name="combinaciones[{$group.name}]"
                                   data-product-attribute="{$id_attribute_group}"
                                   data-key="{$group.name}">
                                        {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                             <option 
                                                  value="{$id_attribute}" 
                                                  title="{$group_attribute.name}" 
                                                  data-value="{$group_attribute.name}" 
                                                  {if $group_attribute.selected} selected="selected"{/if}>
                                                  {$group_attribute.name}
                                             </option>
                                        {/foreach}
                                   </select>
                              {/if}
                         </div>
                    {/if}
               {/foreach}
          </div>
          <div class="clearfix">
               <input class="btn btn-primary float-xs-right" type="submit" value="{l s='Calcular Presupuesto' d='Shop.Theme.Actions'}" />
          </div>
     </form>
</section>