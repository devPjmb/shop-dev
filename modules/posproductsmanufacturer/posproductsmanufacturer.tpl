
	<div class="tab-manufacturers-container-slider">
		<div class="container">					
			<div class ='pos_title'>		
				<h2>{$title}</h2>	
						
			</div> 				
			<div class="manu_content ">
			<div class="pos_content">
				<div class="col col-lg-4 col-md-4 col-xs-12">
					<div class="thumb_manu">
						<ul class="tab_manus owl-carousel"> 
							{$count=0}
							{foreach from=$arrayManufacturers item=productsManufacturer name=productsManufacturer}
									<li  data-title="tabtitle_{$productsManufacturer.id_manufacturer}" rel="tab_{$productsManufacturer.id_manufacturer}" {if $count==0} class="active"{/if} > 
										<div class="manu_thumb">
											<img src="{$link->getManufacturerImageLink($productsManufacturer.id_manufacturer, 'home_default')}" alt="{$productsManufacturer.name_manufacturer}" />
										</div>
									</li>
									{$count= $count+1}
							{/foreach}	
						</ul>
					</div>
				</div>
				<div class="col col-lg-8 col-md-8 col-xs-12">
					<div class="manu_facturer row">
						{$rows= $slider_options.rows}
						{$count=0}					
						{foreach from=$arrayManufacturers item=productsManufacturer name=productsManufacturer}	
						<div id="tab_{$productsManufacturer.id_manufacturer}" class="manu_facturer_tab">
						<div class="manu-item_{$count} owl-carousel"> 					
								{if $productsManufacturer.products}
								{foreach from=$productsManufacturer.products item=product name=myLoop}
									{if $smarty.foreach.myLoop.index % $rows == 0 || $smarty.foreach.myLoop.first }
									<div class="item_manu">
									{/if}
									 <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">					
										<div class="item-inner">
											<div class="img_block">
												{block name='product_thumbnail'}
												<a href="{$product.url}" class="thumbnail product-thumbnail">
												<img class="first-image"
												src = "{$product.cover.bySize.home_default.url}"
												alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
												data-full-size-image-url = "{$product.cover.large.url}"
												>
												{hook h="rotatorImg" product=$product}	
												</a>
												{/block}
												
											</div>
											<div class="product_desc">
												{if isset($product.id_manufacturer)}
												 <div class="manufacturer"><a href="{url entity='manufacturer' id=$product.id_manufacturer }">{$product.manufacturer_name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}</a></div>
												{/if}
												{block name='product_name'}
												  <h1 itemprop="name"><a href="{$product.url}" class="product_name">{$product.name|truncate:50:'...'}</a></h1>
												{/block}
												{block name='product_price_and_shipping'}
												  {if $product.show_price}
													<div class="product-price-and-shipping">
													  {if $product.has_discount}
														{hook h='displayProductPriceBlock' product=$product type="old_price"}
														<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
														<span class="regular-price">{$product.regular_price}</span>
													  {/if}

													  {hook h='displayProductPriceBlock' product=$product type="before_price"}

													  <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
													  <span itemprop="price" class="price">{$product.price}</span>
													  {if $product.has_discount}
														{if $product.discount_type === 'percentage'}
														  <span class="discount-percentage discount-product">{$product.discount_percentage}</span>
														{elseif $product.discount_type === 'amount'}
														  <span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
														{/if}
													  {/if}
													  {hook h='displayProductPriceBlock' product=$product type='unit_price'}

													  {hook h='displayProductPriceBlock' product=$product type='weight'}
													</div>
												  {/if}
												{/block}

											</div>
										</div>
									  </article>
									{if $smarty.foreach.myLoop.iteration % $rows == 0 || $smarty.foreach.myLoop.last  }
									</div>
									{/if}
								{/foreach}
								{else}
									{l s='There is no product from this manufacturer' mod='posproductsmanufacturer'}
								{/if}		
						
						 </div> <!-- .tab_container -->
						 </div>
						 <script type="text/javascript">
						
							$(document).ready(function() {
								var manuSlide = $(".tab-manufacturers-container-slider .manu-item_{$count}");
								manuSlide.owlCarousel({
									autoPlay : 
									{if $slider_options.auto_play} 
										{if $slider_options.auto_time}{$slider_options.auto_time} {else} true {/if} 
									{else} 
										false 
									{/if} ,
									smartSpeed: 200,
									fluidSpeed: {$slider_options.speed_slide},
									navSpeed: {$slider_options.speed_slide},
									animateOut: 'fadeOut',
									animateIn: 'fadeIn',
									autoplayHoverPause: {if $slider_options.pausehover}true{else}false{/if},
									nav: {if $slider_options.show_arrow}true{else}false{/if},
									dots : {if $slider_options.show_pagination}true{else}false{/if},	
									responsive:{
										0:{
											items:{$slider_options.items_xxs},
										},
										544:{
											items:{$slider_options.items_xs},
										},
										768:{
											items:{$slider_options.items_sm},
											nav:false,
										},
										992:{
											items:{$slider_options.items_md},
										},
										1200:{
											items:{$slider_options.number_item},
										}
									}
								});

								
							});

							</script>
						 {$count= $count+1}
						 {/foreach}
					</div> 
				</div> 
			</div>
			</div>
		</div>		
	</div>

<script type="text/javascript">
$(document).ready(function() {
	$(".tab_manus").owlCarousel({
		autoplay : false ,
		smartSpeed: 1000,
		addClassActive: true,
		nav : true,
		dots : false,
		responsive : {
		    0 : {
		        items : 1,
		    },
		    480 : {
		        items : 1, 
		    },
		    768 : {
		        items : 1,
		    },
		    992 : {
		        items : 1,
		    },
		    1200 : {
		        items : 1,
		    }
		}
	});
	  $(".manu_facturer_tab").hide();
	$(".manu_facturer_tab:first").show(); 

	$("ul.tab_manus li").click(function() {
		$("ul.tab_manus li").removeClass("active");
		$(this).addClass("active");
		$(".manu_facturer_tab").hide();
		var activeTab = $(this).attr("rel"); 
		$("#"+activeTab).fadeIn();  
	});
	
});
</script>