
<div class="product-tabs-container-slider parents-product">
	<div class="container">
		<div class="tab_inner">
			<ul class="tabs_slider">
			{$count=0}
			{foreach from=$productTabslider item=productTab name=posTabProduct}
				<li class="{$productTab.id} {if $smarty.foreach.posTabProduct.first}first_item{elseif $smarty.foreach.posTabProduct.last}last_item{else}{/if} item {if $count==0} active {/if}" rel="tab_{$productTab.id}"  >
					<span>{$productTab.name}</span>

				</li>
				{$count= $count+1}
			{/foreach}	
			</ul>
		</div>
		{$rows= $config['POS_HOME_PRODUCTTAB_ROWS']}
		<div class="row pos_content"> 
		{$countss=0}
		{foreach from=$productTabslider item=productTab name=posTabProduct}
				<div id="tab_{$productTab.id}" class="tab_content">
					<div class="productTabContent{$countss} productTab-item owl-carousel shop-products">
					{foreach from=$productTab.productInfo item=product name=myLoop}
					{if $smarty.foreach.myLoop.index % $rows == 0 || $smarty.foreach.myLoop.first }
						<div class="item-product">
					{/if}				
						<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
							<div class="item-inner">
								<div class="img_block">
								  {block name='product_thumbnail'}
									<a href="{$product.url}" class="thumbnail product-thumbnail">
									  <img class="first-image"
										src = "{$product.cover.bySize.home_default.url}"
										alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
										data-full-size-image-url = "{$product.cover.large.url}"
									  >
									   {hook h="rotatorImg" product=$product}	
									</a>
								  {/block}
								</div>
								<div class="product_desc">
									{if isset($product.id_manufacturer)}
									 <div class="manufacturer"><a href="{url entity='manufacturer' id=$product.id_manufacturer }">{$product.manufacturer_name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}</a></div>
									{/if}
									{block name='product_name'}
									  <h1 itemprop="name"><a href="{$product.url}" class="product_name">{$product.name|truncate:50:'...'}</a></h1>
									{/block}
									{block name='product_price_and_shipping'}
									  {if $product.show_price}
										<div class="product-price-and-shipping">
										  {if $product.has_discount}
											{hook h='displayProductPriceBlock' product=$product type="old_price"}
											<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
											<span class="regular-price">{$product.regular_price}</span>
										  {/if}

										  {hook h='displayProductPriceBlock' product=$product type="before_price"}

										  <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
										  <span itemprop="price" class="price">{$product.price}</span>
										  {if $product.has_discount}
											{if $product.discount_type === 'percentage'}
											  <span class="discount-percentage discount-product">{$product.discount_percentage}</span>
											{elseif $product.discount_type === 'amount'}
											  <span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>
											{/if}
										  {/if}
										  {hook h='displayProductPriceBlock' product=$product type='unit_price'}

										  {hook h='displayProductPriceBlock' product=$product type='weight'}
										</div>
									  {/if}
									{/block}
															
								</div>
							</div>
						</article>
					{if $smarty.foreach.myLoop.iteration % $rows == 0 || $smarty.foreach.myLoop.last  }
						</div>
					{/if}	
					{/foreach}
					</div>
				</div>
				{$countss= $countss+1}
		{/foreach}	
		</div>
	</div>
</div>
