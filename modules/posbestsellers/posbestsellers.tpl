
<div class="pos-bestsellers-product">
	<div class="pos_title">
		<h2>{l s='bestsellers' mod='posbestsellers'}</h2>
	</div>
	<div class="block-content">
		<div class="pos_content row">
		{if count($products) > 0 && $products != null}		
			{$rows= 1}
			{if isset($config['POS_HOME_SELLER_ROWS']) && $config['POS_HOME_SELLER_ROWS']>0}
				{$rows= $config['POS_HOME_SELLER_ROWS']}
			{/if}
			<div class="bestsellerSlide owl-carousel">
				{foreach from=$products item=product name=myLoop}
					{if $smarty.foreach.myLoop.index % $rows == 0 || $smarty.foreach.myLoop.first }
						<div class="item-product">
					{/if}
						<article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
							<div class="item-inner">
								<div class="img_block">
								  {block name='product_thumbnail'}
									<a href="{$product.url}" class="thumbnail product-thumbnail">
									  <img class="first-image"
										src = "{$product.cover.bySize.home_default.url}"
										alt = "{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
										data-full-size-image-url = "{$product.cover.large.url}"
									  >
									   {hook h="rotatorImg" product=$product}	
									</a>
								  {/block}
								</div>
								<div class="product_desc">
									{if isset($product.id_manufacturer)}
									 <div class="manufacturer"><a href="{url entity='manufacturer' id=$product.id_manufacturer }">{$product.manufacturer_name|strip_tags:'UTF-8'|escape:'html':'UTF-8'}</a></div>
									{/if}
									{block name='product_name'}
									  <h1 itemprop="name"><a href="{$product.url}" class="product_name">{$product.name|truncate:50:'...'}</a></h1>
									{/block}
									{block name='product_price_and_shipping'}
									  {if $product.show_price}
										<div class="product-price-and-shipping">
										  {if $product.has_discount}
											{hook h='displayProductPriceBlock' product=$product type="old_price"}

											<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>
											<span class="regular-price">{$product.regular_price}</span>
										  {/if}

										  {hook h='displayProductPriceBlock' product=$product type="before_price"}

										  <span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>
										  <span itemprop="price" class="price {if $product.has_discount}price-sale{/if}">{$product.price}</span>
										  {hook h='displayProductPriceBlock' product=$product type='unit_price'}

										  {hook h='displayProductPriceBlock' product=$product type='weight'}
										</div>
									  {/if}
									{/block}
															
								</div>
							</div>
						</article>
					{if $smarty.foreach.myLoop.iteration % $rows == 0 || $smarty.foreach.myLoop.last  }
						</div>
					{/if}
				{/foreach}
			</div>
		{else}
			<p style="padding:20px;">{l s='No best sellers at this time' mod='posbestsellers'}</p>	
		{/if}	
		</div>
	</div>
</div>



